<?php

//returns true if the current user is in a meeting
function _clarity_current_user_in_meeting($meeting_id) {
  global $user;
  if(!empty($user->uid)) {
    $participants = _clarity_get_meeting_participants($meeting_id);
    foreach($participants as $participant) {
      if($participant->uid == $user->uid)
        return true;
    }
  }
  else
    return false;
}

//adds a new meeting participant
function _clarity_add_current_user_to_meeting($meeting_id) {
  global $user;
  if(!empty($user->uid)) {
    if(!_clarity_current_user_in_meeting($meeting_id)) {
      db_insert('clarity_participants')
        ->fields(array('uid' => $user->uid, 'nid' => $meeting_id, 'created' => REQUEST_TIME, 'changed' => REQUEST_TIME))
        ->execute();
    }
  }
}

//returns array of objects, representing meeting participants
function _clarity_get_meeting_participants($meeting_id) {
  return db_select('clarity_participants', 'p')
    ->fields('p')
    ->condition('nid', $meeting_id,'=')
    ->execute()
    ->fetchAllAssoc('pid');
}

//presents the participants of a meeting
function _clarity_show_meeting_participants($participants) {
  if(!empty($participants)) {
    $retval = "";
    foreach($participants as $participant) {
      $user = user_load($participant->uid);
      $image_path = "/" . drupal_get_path('module', 'clarity') . "/img/{$participant->state_id}";
      $state_text = _clarity_get_states();
      $state_text = $state_text[$participant->state_id];
      $user->name = _clarity_trim_name($user->name);
      $retval .= "<div class='participant_state'>{$user->name} <img src='$image_path.svg' alt='{$user->name}: $state_text' title='{$user->name}: $state_text' onerror='this.src=\"$image_path.png\"; this.onerror=null;'><br />" . format_interval((time() - $participant->changed) , 2) . t(' ago') . "</div>";
    }
    return $retval;
  }
}

function _clarity_change_state_links($meeting_id) {

  $clarity_states = _clarity_get_states();

  $retval = "<div class='states'><div class='process_states'><h3>Process states</h3><p><small>Use these states during a discussion</small></p>";

  foreach($clarity_states as $state_id => $state_name) {

    $retval .= "<div class='state'>";

    $img_path = '/' . drupal_get_path('module', 'clarity') . "/img/$state_id";
    $link = "<img src='" . "$img_path.svg' onerror='this.src=\"$img_path.png\"; this.onerror=null;' align='absmiddle' />";
    $retval .= l($link, "change_state/$meeting_id/$state_id", array('html' => true, 'attributes' => array('class' => 'icon', 'onclick' => "return clarityChangeState($meeting_id, $state_id);"))) . " ";
    $retval .= l($state_name, "change_state/$meeting_id/$state_id", array('html' => true, 'attributes' => array('onclick' => "return clarityChangeState($meeting_id, $state_id);"))) . " ";

    $retval .= "</div>";

    if($state_id == 4)
      $retval .= "</div><div class='decision_states'><h3>Decision making states</h3><p><small>Use these after discussion once a decision is being made</small></p>";
  }

  $retval .= '</div></div>';

  return $retval;

}

/**
 * @return array of states
 */
function _clarity_get_states() {
  $clarity_states = array(
    1 => 'Listening',
    2 => 'I want to talk soon',
    3 => 'Let me interrupt - I can help',
    4 => 'Point of order - something is going wrong',
    5 => 'Agree',
    6 => 'Ok, indifferent',
    7 => 'Oppose',
    8 => 'Block',
  );
  return $clarity_states;
}

//outputs a HTML representation of the meeting attendees and their states
function _clarity_format_meeting_attendees($meeting_id) {
  $retval = ""; //the result of this function is collated here

  if(!_clarity_current_user_in_meeting($meeting_id))
    _clarity_add_current_user_to_meeting($meeting_id);

  $attendees = _clarity_get_meeting_participants($meeting_id);

  if(empty($attendees)) {
    $retval = "<p>" . t("No one is here yet") . "</p>";
  }
  else {
    $retval .= _clarity_show_meeting_participants($attendees);
  }

  return $retval;
}

function _clarity_meeting_creator_links($meeting_nid) {
  $retval = '<div class="admin_links">';

  $retval .= "<p><a href='#' onclick='return clarityResetEveryone($meeting_nid)'>Reset everyone to 'listening'</a></p>";

  $retval .= "<p><a href='/node/$meeting_nid/delete'>Delete this meeting</a></p>";

  $retval .= "</div>";

  return $retval;
}

function _clarity_current_user_is_invited( $meeting_id ) {
  global $user;
  $retval = '';

  $nids = db_query("SELECT m.field_meeting_id_value AS nid FROM invite i
                    INNER JOIN field_data_field_meeting_id m ON i.iid = m.entity_id
                    WHERE i.invitee = :uid AND i.canceled = 0", array(':uid' => $user->uid));

  foreach($nids as $nid) {
    if($nid->nid == $meeting_id)
      return true;
  }

  return false;

}

//if the name is long then just return the first name
function _clarity_trim_name($name) {
  if(strlen($name) > 13) {
    $name_parts = explode(' ', $name);
    return $name_parts[0];
  }
  return $name;
}
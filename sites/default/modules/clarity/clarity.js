

function clarityChangeState(meeting_id, state_id) {
	jQuery.ajax({
	  url: "/clarity_ajax/changestate/" + meeting_id + "/" + state_id,
	  cache: false,
	  complete: function( html ) {
	    jQuery( "#meeting_attendees" ).html( html.responseText );
	  }
	});

	return false;
}

function clarityRefreshMeeting(meeting_id) {
	jQuery.ajax({
	  url: "/clarity_ajax/getmeetingstate/" + meeting_id,
	  cache: false,
	  complete: function( html ) {
	    jQuery( "#meeting_attendees" ).html( html.responseText );
	  }
	});

	setTimeout(function() {
	    clarityRefreshMeeting(meeting_id);
	}, 4000);
}

function clarityResetEveryone(meeting_id) {
	jQuery.ajax({
		url: "/clarity_ajax/resetmeetingstate/" + meeting_id,
		cache: false,
		complete: function( html ) {
			alert('Everyone has been set to the listening state');
		}
	});

	return false;
}
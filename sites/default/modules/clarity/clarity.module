<?php

module_load_include('inc', 'clarity', 'clarity.functions');
module_load_include('inc', 'clarity', 'clarity.pages');

function clarity_permission() {
  return array(
    'change own meeting state' => array(
      'title' => t('Change own meeting state')
    ),
  );
}

/**
 * Implements hook_menu().
 */
function clarity_menu() { 
  $items = array();

  $items['my-meetings'] = array(
      'title' => 'My meetings',
      'page callback' => 'clarity_my_meetings',
      'type' => MENU_NORMAL_ITEM,
      'access callback' => 'user_access',
      'access arguments' => array('change own meeting state'),
      'file' => 'clarity.pages.inc',
  );

  $items['change_state/%/%'] = array(
    'title' => 'Change meeting state',
    'page callback' => 'clarity_change_meeting_state',
    'page arguments' => array(1, 2),
    'type' => MENU_NORMAL_ITEM,
    'access callback' => 'user_access',
    'access arguments' => array('change own meeting state'),
    'file' => 'clarity.pages.inc',
  );

  $items['clarity_ajax/changestate/%/%'] = array(
    'title' => 'Change meeting state via ajax',
    'page callback' => 'clarity_change_meeting_state_ajax',
    'page arguments' => array(2, 3),
    'type' => MENU_NORMAL_ITEM,
    'access callback' => 'user_access',
    'access arguments' => array('change own meeting state'),
    'file' => 'clarity.pages.inc',
  );

  $items['clarity_ajax/getmeetingstate/%'] = array(
    'title' => 'Refresh meeting state via ajax',
    'page callback' => 'clarity_refresh_meeting_state_ajax',
    'page arguments' => array(2),
    'type' => MENU_NORMAL_ITEM,
    'access callback' => 'user_access',
    'access arguments' => array('change own meeting state'),
    'file' => 'clarity.pages.inc',
  );

  $items['clarity_ajax/resetmeetingstate/%'] = array(
    'title' => 'Change all to listening',
    'page callback' => 'clarity_reset_meeting_state_ajax',
    'page arguments' => array(2),
    'type' => MENU_NORMAL_ITEM,
    'access callback' => 'user_access',
    'access arguments' => array('change own meeting state'),
    'file' => 'clarity.pages.inc',
  );

  // Invite multiple people
  $items['invite_multiple/%node'] = array(
    'title' => 'Invite multiple people',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('clarity_invite_multiple_form', 1),
    'access callback' => 'clarity_invite_multiple_access',
    'access arguments' => array(1),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'clarity.pages.inc'
  );

  // Minor code reduction technique.
  $base = array(
    'access callback' => 'user_access',
    'access arguments' => array('administer modules'),
  );

  // Admin page
  $items['admin/config/clarityrainbow'] = array(
    'title' => 'Clarity Rainbow settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('clarity_settings_form'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'clarity.pages.inc'
  ) + $base;
  
  return $items;
}

function clarity_node_view($node, $build_mode = 'full') {
  if ($node->type == 'meeting') {
    global $user;

    //ensure people are always participants in their own meetings
    if($node->uid == $user->uid) {
      if(!_clarity_current_user_in_meeting($node->nid))
        _clarity_add_current_user_to_meeting($node->nid);
    }

    //add people who are invited to the meeting
    if( !_clarity_current_user_in_meeting( $node->nid )  && _clarity_current_user_is_invited( $node->nid )) {
      _clarity_add_current_user_to_meeting($node->nid);
    }

    if ( _clarity_current_user_in_meeting( $node->nid ) ) {
      drupal_add_library( 'system', 'drupal.ajax' );

      $node->content['meeting_attendees'] = array(
          '#markup' => _clarity_format_meeting_attendees( $node->nid ),
          '#weight' => 1,
          '#prefix' => '<h2>Meeting participants state</h2><div id="meeting_attendees">',
          '#suffix' => "</div><div class='cf'></div><script>setTimeout(function() {
                                      clarityRefreshMeeting({$node->nid});
                                  }, 4000);</script>",
      );

      $node->content['change_state'] = array(
          '#markup' => _clarity_change_state_links( $node->nid ),
          '#weight' => 2,
          '#prefix' => '<h2>Signal your intentions using the icons below:</h2>',
      );

      //meeting creator can do extra things
      if($node->uid == $user->uid) {
        $node->content['meeting_admin_functions'] = array(
          '#markup' => _clarity_meeting_creator_links( $node->nid ),
          '#weight' => 3,
          '#prefix' => '<h2 style="clear: both; padding-top: 20px;">Meeting creator functions:</h2>',
        );
      }

      $breadcrumb = array();
      $breadcrumb[] = l('Home', '<front>');
      $breadcrumb[] = l('My meetings', 'my-meetings');

      drupal_set_breadcrumb($breadcrumb);

    } else {
      drupal_access_denied();
      exit();
    }
  }
}

/**
 * Implements hook_node_insert().
 */
function clarity_node_insert($node) {
  if($node->type == 'meeting')
    drupal_set_message("Please <a href='/invite_multiple/{$node->nid}'>invite some people to this meeting</a> to grant them access to it.");
}

//hook_invite_accept implementation
function clarity_invite_accept($invite) {

  $meeting_id = $invite->field_meeting_id['und'][0]['value'];

  db_insert('clarity_participants')
      ->fields(array('uid' => $invite->invitee, 'nid' => $meeting_id, 'created' => REQUEST_TIME, 'changed' => REQUEST_TIME))
      ->execute();

}

function clarity_form_alter(&$form, &$form_state, $form_id) {
  switch($form_id) {
    case 'invite_form':
      $form['field_meeting_id']['und'][0]['value']['#default_value'] = arg(1);
      drupal_add_css("#edit-field-meeting-id {display: none;}", 'inline');
      $form['#submit'][] = 'clarity_invite_form_submit';
      $form['actions']['submit']['#submit'][] = 'clarity_invite_form_submit';
      if(isset($form['#redirect'])) unset($form['#redirect']);
      $form['field_multiple_invite_link'] = array(
        '#markup' => '<p>' . l('Add multiple people', 'invite_multiple/' . arg(1)) . '</p>',
        '#weight' => 990,
      );
      break;
    case 'user_register_form':
      drupal_set_title("Please register or log in");
      $form['captcha'] = array(
        '#type' => 'textfield',
        '#title' => 'Please type the number after 1 in this box',
        '#description' => 'Hint: 2',
        '#required' => true,
      );
      $form['#validate'][] = 'clarity_user_register_form_validate';
      break;

    case 'user_pass':
      drupal_set_title("Request new password");
      break;
    case 'user_login':
      drupal_set_title("Login");
      break;
    case 'user_login_block':
      $form['links']['#weight'] = 999;
      $form['links']['#markup'] = '<div class="item-list"><ul><li class="first"><a href="/user/register" title="Create a new user account.">Sign Up</a></li>
        <li class="last"><a href="/user/password" title="Request new password via e-mail.">Forgotten password</a></li>
        </ul></div>';
      if(module_exists('hybridauth')) {
        $form['hybridauth'] = array(
          '#type' => 'hybridauth_widget',
          '#weight' => 1000,
        );
      }
      break;
  }
}

function clarity_user_register_form_validate($form, &$form_state) {
  // Random example, if the title is 'test' throw an error
  if ($form_state['values']['captcha'] != '2') {
    form_set_error('captcha', 'Please type "2" into this field');
  }
}

function clarity_invite_form_submit(&$form, &$form_state) {
  $form_state['redirect'] = 'node/' . $form_state['values']['field_meeting_id']['und'][0]['value'];
}

function clarity_mail_alter(&$message) {
  //Put the person's name into the From: email address
  if($message['id'] == 'invite_by_email_invite') {
    global $user;
    $message['from'] = '"' . $user->name . '" <' . $message['from'] . '>';
    $message['headers']['From'] = $message['from'];
  }
}


<?php

//main list of meetings
function clarity_my_meetings() {
  global $user;
  $retval = '';

  $nids = db_query("SELECT nid FROM node WHERE type='meeting' AND uid = :uid
                    UNION
                    SELECT m.field_meeting_id_value AS nid FROM invite i
                    INNER JOIN field_data_field_meeting_id m ON i.iid = m.entity_id
                    WHERE i.invitee = :uid AND i.canceled = 0", array(':uid' => $user->uid));

  $rows = array();
  foreach($nids as $nid) {
    $node = node_load($nid->nid);
    $row = array(l($node->title, "node/{$node->nid}"),
      format_date($node->created),
      l('view', "node/{$node->nid}"),
      l('edit', "node/{$node->nid}/edit"));
    $rows[] = $row;
  }

  if(!empty($rows))
    return theme('table', array('header' => array('Subject', 'Created', 'View', 'Edit'), 'rows' => $rows));
  else
    return "<p>You have no meetings yet.</p><p><a href='/node/add/meeting'>Create a new meeting</a></p>";
}

/**
 * Page callback: clarity settings
 *
 * @see clarity_menu()
 */
function clarity_settings_form() {
  $form['email_canceled'] = array(
    '#type' => 'fieldset',
    '#title' => t('Account canceled'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('Enable and edit e-mail messages sent to users when their accounts are canceled.') . ' ',
    '#group' => 'email',
  );
  $form['email_canceled']['user_mail_status_canceled_notify'] = array(
    '#type' => 'checkbox',
    '#title' => t('Notify user when account is canceled.'),
    '#default_value' => variable_get('user_mail_status_canceled_notify', FALSE),
  );
  $form['email_canceled']['settings'] = array(
    '#type' => 'container',
    '#states' => array(
      // Hide the settings when the cancel notify checkbox is disabled.
      'invisible' => array(
        'input[name="user_mail_status_canceled_notify"]' => array('checked' => FALSE),
      ),
    ),
  );
  $form['email_canceled']['settings']['user_mail_status_canceled_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#default_value' => _user_mail_text('status_canceled_subject', NULL, array(), FALSE),
    '#maxlength' => 180,
  );
  $form['email_canceled']['settings']['user_mail_status_canceled_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Body'),
    '#default_value' => _user_mail_text('status_canceled_body', NULL, array(), FALSE),
    '#rows' => 3,
  );

  return system_settings_form($form);

}

//form to invite multiple people to the meeting
function clarity_invite_multiple_form($form, &$form_state, $meeting_node) {
  drupal_set_title("Add people to " . $meeting_node->title);
  $form['email_addresses'] = array(
    '#type' => 'textarea',
    '#title' => 'Email addresses',
    '#description' => 'A list of email addresses, either one per line or separated by commas (but not a mixture of both).',
    '#required' => true,
  );

  $form['meeting_id'] = array(
    '#type' => 'value',
    '#value' => $meeting_node->nid
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Send email invitations'
  );

  return $form;
}

//submit handler for the form to invite multiple people to the meeting
function clarity_invite_multiple_form_submit($form, &$form_state) {
  $email_addresses = trim($form_state['values']['email_addresses']);
  $meeting_id = $form_state['values']['meeting_id'];
  $sent = 0; $not_sent = 0;

  $emails_split_by_comma = explode(',', $email_addresses);
  $emails_split_by_line = explode("\n", $email_addresses);

  if(count($emails_split_by_comma) > count($emails_split_by_line)) {
    $email_addresses = $emails_split_by_comma;
  }
  else {
    $email_addresses = $emails_split_by_line;
  }

  foreach($email_addresses as $email_address) {
    $email_address = trim($email_address);
    if(valid_email_address($email_address)) {
      $invite = entity_create('invite', array('type' => 'invite_by_email'));

      $invite->field_invitation_email_address['und'][0]['value'] = $email_address;
      $invite->field_invitation_email_subject['und'][0]['value'] = '[invite:inviter:name] has invited you to a meeting';
      $invite->field_invitation_email_body['und'] = array();
      $invite->field_meeting_id['und'][0]['value'] = $meeting_id;

      invite_save($invite);

      $sent++;
    }
    else {
      $not_sent++;
    }
  }

  if($not_sent) {
    drupal_set_message(t("@not_sent invitations were not sent due to invalid email addresses.", array('@not_sent' => $not_sent)));
  }
}

function clarity_invite_multiple_access($meeting_node) {
  global $user;
  if($meeting_node->uid == $user->uid) {
    return true;
  }
  if(_clarity_current_user_in_meeting($meeting_node->nid))
    return true;

  return false;
}


function clarity_change_meeting_state($meeting_id, $state_id) {
  global $user;
  if(!empty($user->uid)) {
    db_update('clarity_participants')
      ->fields(array(
        'state_id' => $state_id,
        'changed' => REQUEST_TIME,
      ))
      ->condition('nid', $meeting_id, '=')
      ->condition('uid', $user->uid, '=')
      ->execute();

    drupal_goto("node/$meeting_id");
  }
  else
    exit();
}

function clarity_change_meeting_state_ajax($meeting_id, $state_id) {
  global $user;
  if(!empty($user->uid)) {
    db_update('clarity_participants')
      ->fields(array(
        'state_id' => $state_id,
        'changed' => REQUEST_TIME,
      ))
      ->condition('nid', $meeting_id, '=')
      ->condition('uid', $user->uid, '=')
      ->execute();

    clarity_refresh_meeting_state_ajax($meeting_id);
  }

  exit();
}

function clarity_refresh_meeting_state_ajax($meeting_id) {
  echo _clarity_format_meeting_attendees($meeting_id);
  exit();
}

//change all meeting participants to be listening
function clarity_reset_meeting_state_ajax($meeting_id) {
  global $user;

  if(is_numeric($meeting_id)) {
    $meeting = node_load($meeting_id);
    if($meeting->uid == $user->uid) {
      db_update('clarity_participants')
        ->fields(array(
          'state_id' => 1, //Listening state ID
          'changed' => REQUEST_TIME,
        ))
        ->condition('nid', $meeting_id, '=')
        ->execute();
    }
  }
}